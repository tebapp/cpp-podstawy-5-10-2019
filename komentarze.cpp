/*
 To jest przykład komentarza wieloliniowego
 Komentarz ten ZASZE rozpoczyna się znakiem /*
 oraz kończy sekwencją znaków pokazanych po strzałce -> */

/*
 * (aby kontynuować ninjeszy komentarz należało zacząć go
 * ponownie ponieważ wystąpienie jego zakończenia w
 * dowolnym momencie przerywa go - jak stało się powyżej)
 *
 * Przy okazji tutaj mamy przykład komentarza "zdobionego"
 * czyli dodawanej gwiazdki przy okazji nowej linii
 * komentarza. Zdobienia można robić wedle uznania, z
 * dowolnymi znakami oraz dowolną długością.
 *
 * Komentarze przeważnie służą jako opis działania kodu,
 * jego licencji, autora(ów) kodu/projektu oraz
 * informacji o mechaniźmie działania poszczególnych
 * rozkazów zastosowanych w projekcie by później
 * łatwiej się w nim odnaleźć
*/

#include <iostream>

/* To jest główna funkcja pisanego przez nas programu
 * ZAWSZE musi ona coś zwracać do systemu, w którym
 * program został uruchomiony i zawsze jest to liczba
 * (np. całkowita). Funkcja musi mieć nazwę main.
 * Charakterystyczne dla funkcji jest posiadanie okrągłych
 * nawiasów tuż za nazwą (później można w nich umieszczać
 * parametry - omówione następnym razem) */
int main()
{
    //To jest komentarz jednoliniowy; CAŁA linia po jego wystąpieniu staje się komentarzem; następna to standardowy kod
    //(chyba, że postawimy kolejny raz dwa znaki //)
    std::cout << "Witaj w programowaniu \r"
              << "jest super!\t"
              << " Kolejny tekst w lini!" << std::endl
              << "I kolejny w nowej lini!\n";
    //to jest WYMAGANA linika kodu, która wskazuje
    //na kod zakończenia aplikacji (0 oznacza poprawne zakończenie)
    return 0;
}
