#include <iostream>
//biblioteka niezbedna do operacji matematycznych
#include <cmath>


int main()
{
    int a;
    int b;
    //int a,b;
    //ZMIENNE ZAWSZE POWINNY BYĆ INICJALIZOWANE (posiadać
    //podaną przez programiste wartość początkową). W innym razie
    //mogą zawierać niechciane, losowe wartości z pamięci RAM.
    //WARTO SPRAWDZIĆ SAMEMU poprzez stworzenie zmiennej i
    //wyświetlenie jej poprzez cout
    //INICJALIZACJA:
    //int a=0;
    //int b = 0;
    //lub
    //int a=0,b=0;

    //w tej zmiennej bedziemy przechowywać operator arytmetyczny
    //do obsługi naszego kalkulatora; użytkownik powinien podać
    //jeden z następujących symboli:
    // + - dodawanie
    // - - odejmowanie
    // * - mnożenie
    // / - dzielenie
    // ^ - potegowanie
    // ` - pierwiastek
    char operacja;

    std::cout << "Witaj w moim kalkulatorze:\n";
    std::cout << "Podaj pierwsza wartosc: ";
    std::cin >> a;
    std::cout << "Podaj druga wartosc: ";
    std::cin >> b;
    std::cout << "Podaj rodzaj operacji: ";
    std::cin >> operacja;

    //na razie nie znamy warunkow - wyswietlimy co pobralismy od
    //uzytkownika
    std::cout << "Rownanie ma postac: " << a << operacja << b <<
                 " = \nDziekuje za skorzystanie z programu!\n";

    //najprostszy z możliwych warunków w programowaniu
    // if (warunek) {
    //     //kod do wykonania
    // }
    // Warunkiem jest osiągnięcie z postawionego równania
    // logicznego wartości prawdy (true) - inaczej mówiąc
    // wartości innej niż 0
    // Warunki logiczne dostępne w programowaniu:
    // a < b - prawda, jeżeli wartość a będzie mniejsza od b
    // a > b - prawda, jeżeli wartość a będzie większa od b
    // a <= b - prawda, jeżeli wartość a będzie mniejsza bądź równa b
    // a >= b - prawda, jeżeli wartość a będzie większa bądź równa b
    // a == b - prawda, jeżeli a i b będzzie miało tę samą wartość
    // a != b - prawda, jeżeli wartość a będzie różna od wartości b (dowolna różnica)
    // !a - zamienia wartość a (odrwaca jej wartość)

    //należy pamiętać, że operacje wykonywane są na BITACH/BAJTACH
    //nie zaś na wartościach, jakie my widzimy!

    // Przykład działania if: (sprawdzamy, czy użytkownik nie wpisał
    // znak + jako operacji do wykonania)
    if (operacja == '+') {
        std::cout << "Sumowanie: " << a << ' ' << operacja << ' ' <<
                     b << " = " << (a+b) << '\n';
    }

    // kolejny przykład warunków:
    // if (warunek)
    // {
    //      kod do wykonania gdy spełniony
    // }
    // else
    // {
    //      kod wykona się, jeżeli warunek nie zostanie spełniony
    // }

    // Przykład:

    if (operacja == '-') {
        std::cout << "odejmowanie: " << a << ' ' << operacja << ' ' <<
                     b << " = " << (a-b) << '\n';
    }
    else {
        std::cout << "Niestety, nie rozpoznaje operacji!\n";
    }

    // ostatni przykład warunku:
    // if(warunek)
    // {
    //      kod wykona się gdy warunek będzie spełniony
    // }
    // else if (warunek)
    // {
    //      kod wykona się, gdy poprzedni warunek nie został
    //      spełniony, jednak nowo postawiony warunek spełnia zależność
    // }
    // else if () {... można podać dowolną ilość else if w naszych warunkach }
    // else
    // {
    //      kod wykona się gdy żaden z warunków nie zostanie spełniony
    // }
    // WAŻNE! Klauzula else nie musi być podawana przy operacjach
    // warunkowych (jest opcjonalna)

    //Przykład:

    if (operacja == '*') {
        std::cout << "Mnozenie: " << a << ' ' << operacja << ' ' <<
                     b << " = " << (a*b) << '\n';
    }
    else if (operacja == '/') {
        //tutaj powinniśmy sprawdzić czy nie dzielmy przez zero!
        //warunki można zagnieżdżać!
        if (b != 0) {
            std::cout << "Dzielenie: " << a << ' ' << operacja << ' ' <<
                     b << " = " << (a/b) << '\n';
        }
        else {
            std::cout << "Nie mozna dzielic przez 0!";
        }
    }
    else if (operacja == '^') {
        std::cout << "Potegowanie: " << a << ' ' << operacja << ' ' <<
                     b << " = " << pow(a,b) << '\n';
    }
    else if (operacja == '`') {
        std::cout << "Pierwiastkowanie: " << a << ' ' << operacja << ' ' <<
                     b << " = " << pow(a,1/(b*1.0)) << '\n';
    }
    //należy pamiętać, że jeżeli chcemy uzyskać liczbę
    //zmiennoprzecinkową z liczby całkowitej zapisanej w
    //zmiennej to koniecznie musimy wykonać wymnożenie takiej
    //liczby całkowitej przez 1.0; tym samym zmieniamy
    //niejawnie (czyli bez jednoznacznego działania)
    //typ int na float/double, a tym samym wynik też
    //uzyskamy zmiennoprzecinkowy
    //std::cout << 1/(b*1.0) << "<-wynik dzielenia 1/b\n";

    return 0;
}
